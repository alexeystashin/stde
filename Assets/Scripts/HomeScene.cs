﻿using TdGame.Data;
using TdGame.Impl;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Stde
{
    public class HomeScene : MonoBehaviour
    {
        public Button playEasyButton;
        public Button playHardButton;

        void Start()
        {
            if(playEasyButton != null)
                playEasyButton.onClick.AddListener(StartEasyGame);
            if(playHardButton != null)
                playHardButton.onClick.AddListener(StartHardGame);
        }

        private void StartEasyGame()
        {
            var rules = FakeStaticGameData.CreateEasyGameRules();
            LaunchGame(rules);
        }

        private void StartHardGame()
        {
            var rules = FakeStaticGameData.CreateHardGameRules();
            LaunchGame(rules);
        }

        private void LaunchGame(GameRules rules)
        {
            GameScene.gameRules = rules;
            GameScene.returnScene = "home";

            SceneManager.LoadScene("game");
        }
    }
}
