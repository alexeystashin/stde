﻿using TdGame.Data;
using TdGame.Impl;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Stde
{
    public class GameScene : MonoBehaviour
    {
        public static GameRules gameRules;
        public static string returnScene;

        void Start()
        {
            var game = FindObjectOfType<Game>();
            if (game != null)
            {
                Debug.LogWarning("Other game detected");
                return;
            }

            LaunchTestGame();
        }

        private void LaunchTestGame()
        {
            var go = new GameObject("Game");
            var game = go.AddComponent<Game>();
            game.Initialize(gameRules);
            game.onGameFinished += OnGameFinished;

            //game.StartGame();
        }

        private void OnGameFinished()
        {
            if(!string.IsNullOrEmpty(returnScene))
                SceneManager.LoadScene(returnScene);
        }
    }
}
