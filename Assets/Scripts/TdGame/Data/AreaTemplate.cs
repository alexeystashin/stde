﻿namespace TdGame.Data
{
    public class AreaTemplate
    {
        public string id;

        public string prefabPath;

        public string actionType; // "Damage" (default) or "Freeze"

        public float size;

        public float lifetime;

        public float attackPower;

        public float actionCooldown;

    }
}
