﻿namespace TdGame.Data
{
    public class TowerTemplate
    {
        public string id;

        public string prefabPath;

        public float size;

        public float health;

        public string attackBoltId;

        public float attackCooldown;

    }
}
