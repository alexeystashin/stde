﻿using UnityEngine;

namespace TdGame.Data
{
    // todo: make all these values configurable from outside
    public class MagicNumbers
    {
        public const int EasyWavesCount = 5;
        public const int HardWavesCount = 10;

        public const int LineCount = 3;
        public const int MaxTowerRowCount = 2;

        public const float FieldSizeZ = 32;
        public const float FieldSizeX = 12;
        public const float LineWidth = 4;

        public const float MinLineX = -LineWidth/2.0f;
        public const float MaxLineX = LineWidth/2.0f;

        public const float TowerPosZFirst = 2;
        public const float TowerPosZStep = 4;
        public const float SpawnPosZ = 28;

        public const float BoltY = 1;

        public const float TowerSwapTime = 0.25f;

        public static float GetLineX(int lineId)
        {
            return ((lineId % LineCount) - (LineCount - 1) / 2.0f) * LineWidth;
        }

        public static float GetTowerPosZ(int rowId)
        {
            return TowerPosZFirst + rowId * TowerPosZStep;
        }

        public static Vector3 GetTowerPos(int lineId, int rowId)
        {
            return new Vector3(GetLineX(lineId), 0, GetTowerPosZ(rowId));
        }
    }
}
