﻿using System.Collections.Generic;

namespace TdGame.Data
{
    public class GameResult
    {
        public bool win;
        public int score;
        public int lastWave;
    }
}
