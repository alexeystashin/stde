﻿using System;
using TdGame.Data;
using UnityEngine;

namespace TdGame.Interfaces
{
    public interface IEntityBuilder : IDisposable
    {
        bool isDisposed { get; }

        void Initialize(IGame game);

        ITower CreateTower(string templateId, int lineId, int rowId);
        ICreature CreateCreature(string templateId, int lineId, Vector3 pos);
        IBolt CreateBolt(string templateId, int lineId, Vector3 pos);
        IArea CreateArea(string templateId, int lineId, Vector3 pos);
        ICreatureSpawner CreateSpawner(SpawnerTemplate entityTemplate, int lineId);
    }
}
