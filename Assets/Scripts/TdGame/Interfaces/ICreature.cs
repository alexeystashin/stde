﻿using TdGame.Data;

namespace TdGame.Interfaces
{
    public interface ICreature : IEntity
    {
        CreatureTemplate template { get; }

        bool ApplyDamage(float attackPower);
    }
}
