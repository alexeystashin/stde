﻿using TdGame.Data;

namespace TdGame.Interfaces
{
    public interface IBolt : IEntity
    {
        BoltTemplate template { get; }
    }
}
