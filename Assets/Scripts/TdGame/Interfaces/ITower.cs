﻿using TdGame.Data;

namespace TdGame.Interfaces
{
    public interface ITower : IEntity
    {
        TowerTemplate template { get; }

        int rowId { get; }

        bool TryShoot();

        void ChangePosition(int lineId, int rowId);
        bool ApplyDamage(float attackPower);
    }
}
