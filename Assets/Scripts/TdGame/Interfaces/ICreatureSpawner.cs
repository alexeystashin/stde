﻿using System;

namespace TdGame.Interfaces
{
    public interface ICreatureSpawner : IDisposable
    {
        bool isDisposed { get; }

        int lineId { get; }
    }
}
