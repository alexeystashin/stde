﻿using System;
using TdGame.Data;

namespace TdGame.Interfaces
{
    public interface IGame : IDisposable
    {
        bool isRunning { get; }
        bool isDisposed { get; }

        GameRules gameRules { get; }
        GameResult gameResult { get; }

        int score { get; set; }

        IWaveManager spawnWaves { get; }

        StaticGameData staticGameData { get; }

        void Initialize(GameRules rules = null);
        //void StartGame();
        void FinishGame(bool win);

        IEntityBuilder entityBuilder { get; }
        IEntityManager entityManager { get; }

        IGameUI gameUI { get; }

        ITower GetTower(int lineId, int rowId);
    }
}
