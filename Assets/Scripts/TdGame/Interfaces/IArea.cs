﻿using TdGame.Data;

namespace TdGame.Interfaces
{
    public interface IArea : IEntity
    {
        AreaTemplate template { get; }

        float lifetime { get; }

    }
}
