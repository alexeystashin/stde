﻿using System;
using System.Collections.Generic;
using TdGame.Data;

namespace TdGame.Interfaces
{
    public interface IWaveManager : IDisposable
    {
        void Initialize(IGame game, List<WaveTemplate> waveTemplates);

        int totalWavesCount { get; }

        int currentWave { get; }
        float currentWaveTime { get; }
        float currentWaveTimeTotal { get; }

        bool isAllWavesComplete { get; }
    }
}
