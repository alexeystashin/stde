﻿using System;
using UnityEngine;

namespace TdGame.Interfaces
{
    public interface IGameUI : IDisposable
    {
        Canvas canvas { get; }
        Transform transform { get; }

        void Initialize(IGame game, Canvas canvas);
    }
}
