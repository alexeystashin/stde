﻿using System;
using System.Collections.Generic;

namespace TdGame.Interfaces
{
    public interface IEntityManager : IDisposable
    {
        bool isDisposed { get; }

        Dictionary<EntityType, List<IEntity>> entities { get; }

        void Initialize(IGame game);

        void AddEntity(IEntity entity);
        void RemoveEntity(IEntity entity);
    }
}
