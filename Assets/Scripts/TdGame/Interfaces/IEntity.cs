﻿using System;

namespace TdGame.Interfaces
{
    public interface IEntity : IDisposable
    {
        event Action<IEntity> onDisposed;

        int id { get; }

        EntityType entityType { get; }

        bool isDisposed { get; }

        int lineId { get; }

        float posZ { get; }
    }
}
