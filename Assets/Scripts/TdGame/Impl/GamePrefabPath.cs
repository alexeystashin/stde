﻿using System.Collections.Generic;

namespace TdGame.Impl
{
    // todo: make all these values configurable from outside
    public class GamePrefabPath
    {
        public const string MachineGunTower = "Prefabs/Towers/MachineGunTower";
        public const string RocketTower = "Prefabs/Towers/RocketTower";

        public const string CreepCreature = "Prefabs/Enemies/Hovercopter";
        public const string DamagerCreature = "Prefabs/Enemies/Hovertank";
        public const string TankCreature = "Prefabs/Enemies/Hoverboss";

        public const string BulletBolt = "Prefabs/Bullet";
        public const string RocketBolt = "Prefabs/RocketProjectile";

        public const string ExplosionFx = "Prefabs/ExplosionFx";

        //public const string GameUI = "Prefabs/UI/GameUI";
        public const string GamePauseUI = "Prefabs/UI/GamePauseUI";
        public const string GameResultUI = "Prefabs/UI/GameResultUI";

        public const string ReloadUI = "Prefabs/UI/ReloadUI";
        public const string SmallHealthUI = "Prefabs/UI/SmallHealthUI";
        public const string BigHealthUI = "Prefabs/UI/BigHealthUI";
    }
}
