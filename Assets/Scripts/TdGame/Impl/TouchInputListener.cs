﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//namespace Common
//{
    public class TouchInputListener : MonoBehaviour
    {
        // const
        public const int maxLastTouchPoints = 10;

        // callback actions
        public Action OnTouchDownCallback = () => { };
        public Action OnClickCallback = () => { };
        public Action OnDragStartedCallback = () => { };
        public Action OnDragCallback = () => { };
        public Action OnDragFinishedCallback = () => { };

        // current touch data
        public bool isTouched { get; private set; }
        public bool isDragging { get; private set; }

        public Vector2 startTouchPoint { get; private set; }

        public Vector2 currentTouchPoint { get; private set; }
        public Vector2 currentTouchVector { get; private set; }

        private LinkedList<Vector3> lastTouchPoints = new LinkedList<Vector3>(); // x, y, deltaTime

        public List<Vector3> GetLastTouchPoints()
        {
            return new List<Vector3>(lastTouchPoints);
        }

        void OnDisable()
        {
            StopCurrentTouch(false);
        }

        void Update()
        {
            if (Input.GetMouseButtonDown(0)) // touch started
            {
                isTouched = true;
                startTouchPoint = Input.mousePosition;
                UpdateTouchPoint();

                OnTouchDownCallback();
            }
            else if (Input.GetMouseButton(0)) // touch continues
            {
                if (isDragging)
                {
                    UpdateTouchPoint();

                    OnDragCallback();
                }
                else if (isTouched)
                {
                    UpdateTouchPoint();

                    if (currentTouchVector.magnitude < EventSystem.current.pixelDragThreshold * 0.5f)
                        return;

                    isDragging = true;

                    OnDragStartedCallback();
                }
            }
            else if (isTouched) // touch ended
            {
                UpdateTouchPoint();
                StopCurrentTouch(true);
            }
        }

        private void StopCurrentTouch(bool canClick)
        {
            if (!isTouched)
                return;

            if (isDragging)
            {
                OnDragFinishedCallback();
            }
            else if (canClick && isTouched)
            {
                OnClickCallback();
            }

            isTouched = false;
            isDragging = false;

            lastTouchPoints.Clear();
        }

        private void UpdateTouchPoint()
        {
            currentTouchPoint = Input.mousePosition;
            currentTouchVector = currentTouchPoint - startTouchPoint;

            if (lastTouchPoints.Count >= maxLastTouchPoints)
                lastTouchPoints.RemoveFirst();

            var lastTouchPoint = new Vector3(currentTouchPoint.x, currentTouchPoint.y, Time.deltaTime);
            lastTouchPoints.AddLast(lastTouchPoint);
        }

        private void ClearCallbacks()
        {
            OnTouchDownCallback = null;
            OnClickCallback = null;
            OnDragStartedCallback = null;
            OnDragCallback = null;
            OnDragFinishedCallback = null;
        }

        public void Dispose()
        {
            ClearCallbacks();
            StopCurrentTouch(false);
        }
    }
//}
