﻿using System.Linq;
using TdGame.Data;
using TdGame.Interfaces;
using UnityEngine;

namespace TdGame.Impl
{
    public class GoalController : MonoBehaviour
    {
        private IGame game;

        public void Initialize(IGame game)
        {
            this.game = game;
        }

        void Update()
        {
            if (game == null || !game.isRunning)
                return;

            CheckForWin();
            CheckForLose();
        }

        // check that all waves are complete && all creatures are dead
        void CheckForWin()
        {
            if (game.gameResult != null)
                return;

            if(game.spawnWaves.isAllWavesComplete)
                game.FinishGame(true);
        }

        // check that all lines have towers
        void CheckForLose()
        {
            if (game.gameResult != null)
                return;

            for (var i = 0; i < MagicNumbers.LineCount; i++)
            {
                var lineHaveTowers = game.entityManager.entities[EntityType.Tower].Any(t => t.lineId == i);
                if (!lineHaveTowers)
                {
                    game.FinishGame(false);
                    break;
                }
            }
        }

        public void Dispose()
        {
            if (game == null)
                return;

            game = null;
        }
    }
}
