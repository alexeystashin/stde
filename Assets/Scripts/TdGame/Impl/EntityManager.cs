﻿using System;
using System.Collections.Generic;
using TdGame.Interfaces;
using UnityEngine;

namespace TdGame.Impl
{
    public class EntityManager : MonoBehaviour, IEntityManager
    {
        public bool isDisposed { get; private set; }

        public Dictionary<EntityType, List<IEntity>> entities { get; private set; }

        private IGame game;

        public void Initialize(IGame game)
        {
            this.game = game;
            entities = new Dictionary<EntityType, List<IEntity>>();
        }

        public void AddEntity(IEntity entity)
        {
            if (!entities.ContainsKey(entity.entityType))
                entities.Add(entity.entityType, new List<IEntity>());
            entities[entity.entityType].Add(entity);

            entity.onDisposed += OnEntityDisposed;
        }

        private void OnEntityDisposed(IEntity entity)
        {
            RemoveEntity(entity);
        }

        public void RemoveEntity(IEntity entity)
        {
            //Debug.Log("RemoveEntity "+ entity.id);
            if (!entities.ContainsKey(entity.entityType))
                throw new Exception();
            entities[entity.entityType].Remove(entity);

            entity.onDisposed -= OnEntityDisposed;
        }

        public virtual void Dispose()
        {
            if (isDisposed)
                return;

            isDisposed = true;

            // cleanup here

            game = null;
        }
    }
}
