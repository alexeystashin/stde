﻿using System;
using System.Linq;
using TdGame.Data;
using TdGame.Interfaces;
using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

namespace TdGame.Impl
{
    public class Game : MonoBehaviour, IGame
    {
        public event Action onGameFinished = () => { };

        public bool isRunning { get; private set; }

        public bool isDisposed { get; private set; }

        public GameRules gameRules { get; private set; }
        public GameResult gameResult { get; private set; }

        public StaticGameData staticGameData { get; private set; }

        public IEntityBuilder entityBuilder { get; private set; }

        public IEntityManager entityManager { get; private set; }

        public IGameUI gameUI { get; private set; }

        public IWaveManager spawnWaves { get; private set; }

        public PlayerController playerController { get; private set; }

        public int score { get; set; }

        private GoalController goalController;

        private GameResultUI gameResultUI;
        private GamePauseUI gamePauseUI;

        void Start()
        {
            StartGame();
        }

        public void Initialize(GameRules rules = null)
        {
            staticGameData = FakeStaticGameData.Create(); // todo: tmp

            gameRules = rules ?? FakeStaticGameData.CreateEasyGameRules();

            var go = new GameObject("Entities");
            go.transform.SetParent(transform, false);

            entityBuilder = go.AddComponent<EntityBuilder>();
            entityBuilder.Initialize(this);

            entityManager = go.AddComponent<EntityManager>();
            entityManager.Initialize(this);

            go = new GameObject("SpawnWaves");
            go.transform.SetParent(transform, false);
            spawnWaves = go.AddComponent<WaveSpawnerManager>();
            spawnWaves.Initialize(this, gameRules.waves);

            go = new GameObject("PlayerController");
            go.transform.SetParent(transform, false);
            playerController = go.AddComponent<PlayerController>();
            playerController.Initialize(this);

            goalController = go.AddComponent<GoalController>();
            goalController.Initialize(this);
        }

        private void InitUI()
        {
            // todo: remove hardcode

            var canvas = FindObjectOfType<Canvas>();

            if(canvas == null)
                throw new Exception("canvas not found");

            var go = canvas.transform.Find("GameUI").gameObject;
            gameUI = go.AddComponent<GameUI>();
            gameUI.Initialize(this, canvas);
            ((GameUI)gameUI).onMenuButtonClicked += OnMenuButtonClicked;

            var prefab = Resources.Load<GameObject>(GamePrefabPath.GamePauseUI);
            go = Instantiate(prefab);
            go.transform.SetParent(canvas.transform, false);
            gamePauseUI = go.GetComponent<GamePauseUI>();
            gamePauseUI.onHomeButtonClicked += OnHomeButtonClicked;
            gamePauseUI.onRestartButtonClicked += OnRestartButtonClicked;
            gamePauseUI.onContinueButtonClicked += OnContinueButtonClicked;
            go.SetActive(false);

            prefab = Resources.Load<GameObject>(GamePrefabPath.GameResultUI);
            go = Instantiate(prefab);
            go.transform.SetParent(canvas.transform, false);
            gameResultUI = go.GetComponent<GameResultUI>();
            gameResultUI.onRestartButtonClicked += OnRestartButtonClicked;
            gameResultUI.onContinueButtonClicked += OnGameResultUIComplete;
            go.SetActive(false);
        }

        private void CreateTowers()
        {
            var l = 0;
            var r = 0;
            foreach (var towerId in gameRules.playerTowers)
            {
                var tower = entityBuilder.CreateTower(towerId, l, r);
                entityManager.AddEntity(tower);

                l++;
                if (l >= MagicNumbers.LineCount)
                {
                    l = 0;
                    r++;
                }
            }
        }

        private void StartGame()
        {
            Assert.IsFalse(isRunning);

            InitUI();
            CreateTowers();

            isRunning = true;
        }

        public void FinishGame(bool win)
        {
            Assert.IsTrue(isRunning);
            Assert.IsNull(gameResult);

            isRunning = false;

            gameResult = new GameResult
            {
                win = win,
                score = score,
                lastWave = spawnWaves.currentWave,
            };

            Debug.Log("GAME FINISHED. " + win);

            gameResultUI.gameObject.SetActive(true);

            gameResultUI.winCaption.SetActive(win);
            gameResultUI.loseCaption.SetActive(!win);

            gameResultUI.scoreText.text = score.ToString();
            gameResultUI.waveText.text = (spawnWaves.currentWave+1) + "/" + spawnWaves.totalWavesCount;

            //onGameFinished.Invoke();
        }

        public ITower GetTower(int lineId, int rowId)
        {
            var tower = entityManager.entities[EntityType.Tower]
                .FirstOrDefault(e => e.lineId==lineId && ((ITower) e).rowId == rowId)
                as ITower;

            if(tower == null)
                Debug.LogWarning("Tower not found " + lineId + "," + rowId);
            return tower;
        }

        private void OnMenuButtonClicked()
        {
            if (!isRunning)
                return;

            isRunning = false;

            gamePauseUI.gameObject.SetActive(true);
        }

        private void OnGameResultUIComplete()
        {
            onGameFinished.Invoke();
        }

        private void OnHomeButtonClicked()
        {
            isRunning = true;
            gamePauseUI.gameObject.SetActive(false);

            FinishGame(false);
        }

        private void OnRestartButtonClicked()
        {
            //throw new NotImplementedException();
        }

        private void OnContinueButtonClicked()
        {
            isRunning = true;
            gamePauseUI.gameObject.SetActive(false);
        }

        public void Dispose()
        {
            if (isDisposed)
                return;

            isDisposed = true;

            isRunning = false; // todo: stop via StopGame() method

            if (goalController != null)
            {
                goalController.Dispose();
                goalController = null;
            }

            if (spawnWaves != null)
            {
                spawnWaves.Dispose();
                spawnWaves = null;
            }

            if (playerController != null)
            {
                playerController.Dispose();
                playerController = null;
            }

            if (entityManager != null)
            {
                entityManager.Dispose();
                entityManager = null;
            }

            if (entityBuilder != null)
            {
                entityBuilder.Dispose();
                entityBuilder = null;
            }
        }
    }
}
