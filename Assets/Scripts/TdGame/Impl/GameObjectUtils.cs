﻿using System;
using UnityEngine;

namespace Pump.Unity
{
    public static class GameObjectUtils
    {
        // return full GameObject path in hierarchy 
        public static string GetGameObjectPath(GameObject obj)
        {
            if (obj == null) return "null";

            var path = "/" + obj.name;
            while (obj.transform.parent != null)
            {
                obj = obj.transform.parent.gameObject;
                path = "/" + obj.name + path;
            }
            return path;
        }

        public static string GetGameObjectPath(Component objComponent)
        {
            if (objComponent == null) return "null";
            return GetGameObjectPath(objComponent.gameObject);
        }

        public static void ResetTransform(this Transform transform)
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.Euler(Vector3.zero);
            transform.localScale = Vector3.one;
        }

        // растягивает RectTransform на максимум
        public static void StretchRectTransform(this RectTransform rectTransform)
        {
            rectTransform.localPosition = Vector3.zero;
            rectTransform.localRotation = Quaternion.Euler(Vector3.zero);
            rectTransform.localScale = Vector3.one;

            rectTransform.anchorMin = Vector2.zero;
            rectTransform.anchorMax = Vector2.one;
            rectTransform.offsetMin = Vector2.zero;
            rectTransform.offsetMax = Vector2.zero;
        }

        public static void SetLayerRecursive(this GameObject go, int layer)
        {
            go.layer = layer;
            var count = go.transform.childCount;
            for (var i = 0; i < count; i++)
            {
                var t = go.transform.GetChild(i);
                t.gameObject.layer = layer;
                if (t.childCount > 0)
                    SetLayerRecursive(t.gameObject, layer);
            }
        }

        public static T CreateObject<T>(string name, Transform parent=null) where T : MonoBehaviour
        {
            var go = new GameObject(name);
            if(parent != null)
                go.transform.SetParent(parent,false);
            var component = go.AddComponent<T>();
            return component;
        }

        public static string FormatTime(int totalSeconds)
        {
            var timeSpan = TimeSpan.FromSeconds(totalSeconds);

            var days = (int)Math.Floor(timeSpan.TotalDays);
            var hours = (int)Math.Floor((double)timeSpan.Hours);
            var minutes = (int)Math.Floor((double)timeSpan.Minutes);
            var seconds = (int)Math.Floor((double)timeSpan.Seconds);

            string resultStr = string.Empty;

            if (totalSeconds == 0)
                return "00:00";

            if (days > 0)
                resultStr = string.Format("{0}:{1}", days, hours);
            else if (hours > 0)
                resultStr = string.Format("{0}:{1}", hours, minutes);
            else if (minutes > 0)
                resultStr = string.Format("{0}:{1}", minutes.ToString("00"), seconds.ToString("00"));
            else
                resultStr = "00:" + seconds.ToString("00");

            return resultStr;
        }
    }
}
