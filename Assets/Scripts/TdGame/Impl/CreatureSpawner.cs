﻿using TdGame.Data;
using TdGame.Interfaces;
using UnityEngine;

namespace TdGame.Impl
{
    public class CreatureSpawner : MonoBehaviour, ICreatureSpawner
    {
        public bool isDisposed { get; private set; }

        public int lineId { get; private set; }

        private SpawnerTemplate template;

        private float lifetime;

        private float spawnCooldownTime;

        private IGame game;

        public void Dispose()
        {
            if (isDisposed)
                return;

            isDisposed = true;

            // cleanup here

            template = null;
            game = null;
        }

        public void Initialize(IGame game, SpawnerTemplate template, int lineId)
        {
            this.game = game;
            this.template = template;
            this.lineId = lineId;

            lifetime = template.lifetime + template.delay;
            spawnCooldownTime = template.delay;

            transform.position = new Vector3(MagicNumbers.GetLineX(lineId), 0, MagicNumbers.SpawnPosZ);
        }

        protected virtual void Update()
        {
            if (isDisposed) return;

            spawnCooldownTime -= Time.deltaTime;

            if (spawnCooldownTime <= 0)
            {
                SpawnCreature();
            }

            lifetime -= Time.deltaTime;

            if(lifetime <= 0)
                Dispose();
        }

        private void SpawnCreature()
        {
            spawnCooldownTime = Random.Range(template.cooldownMin, template.cooldownMax);

            var randX = Random.Range(-MagicNumbers.LineWidth * 0.3f, MagicNumbers.LineWidth * 0.3f);
            var pos = new Vector3(MagicNumbers.GetLineX(lineId) + randX, 0, MagicNumbers.SpawnPosZ);

            var creature = game.entityBuilder.CreateCreature(template.creatureId, lineId, pos);
            game.entityManager.AddEntity(creature);
        }
    }
}
