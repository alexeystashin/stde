﻿using System.Collections.Generic;
using TdGame.Data;
using TdGame.Interfaces;
using UnityEngine;

namespace TdGame.Impl
{
    public class WaveSpawnerManager : MonoBehaviour, IWaveManager
    {
        public bool isDisposed { get; private set; }

        //public bool endlessMode { get; private set; }

        public int totalWavesCount
        {
            get { return waveTemplates.Count; }
        }

        public int currentWave { get; private set; }
        public float currentWaveTime { get; private set; }
        public float currentWaveTimeTotal { get; private set; }

        public List<WaveTemplate> waveTemplates;

        public bool isAllWavesComplete { get; private set; }

        private List<ICreatureSpawner> spawners = new List<ICreatureSpawner>();

        private IGame game;

        public void Initialize(IGame game, List<WaveTemplate> waveTemplates)
        {
            this.game = game;
            this.waveTemplates = new List<WaveTemplate>(waveTemplates);
            currentWave = 0;

            if (waveTemplates.Count == 0)
            {
                Debug.LogWarning("Empty waves list");
                isAllWavesComplete = true;
                return;
            }
            CreateWaveSpawners(currentWave);
        }

        void Update()
        {
            if (isAllWavesComplete)
                return;

            if (!game.isRunning)
                return;

            currentWaveTime += Time.deltaTime;

            ClearDeadSpawners();

            if (spawners.Count > 0)
                return;

            if (currentWave >= waveTemplates.Count - 1)
            {
                // todo: if endlessMode start new cycle
                isAllWavesComplete = true;
                //Debug.LogWarning("isAllWavesComplete");
                return;
            }

            currentWave++;
            CreateWaveSpawners(currentWave);
        }

        private void CreateWaveSpawners(int waveId)
        {
            currentWaveTime = 0;
            currentWaveTimeTotal = 0;

            var waveTemplate = waveTemplates[waveId];

            for (var l = 0; l < MagicNumbers.LineCount; l++)
            {
                var lineSpawners = waveTemplate.lineSpawners[l];
                foreach (var spawnerTemplate in lineSpawners)
                {
                    var spawner = game.entityBuilder.CreateSpawner(spawnerTemplate, l);
                    spawners.Add(spawner);

                    currentWaveTimeTotal = Mathf.Max(currentWaveTimeTotal,
                        spawnerTemplate.delay + spawnerTemplate.lifetime);
                }
            }
        }

        private void ClearDeadSpawners()
        {
            for (var i = 0; i < spawners.Count; i++)
            {
                if (spawners[i].isDisposed)
                {
                    spawners.RemoveAt(i);
                    i--;
                }
            }
        }

        public virtual void Dispose()
        {
            if (isDisposed)
                return;

            isDisposed = true;

            // cleanup here

            game = null;
        }
    }
}
