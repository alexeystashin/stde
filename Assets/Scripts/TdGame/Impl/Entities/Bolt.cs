﻿using System.Linq;
using TdGame.Data;
using TdGame.Interfaces;
using UnityEngine;

namespace TdGame.Impl
{
    public class Bolt : Entity, IBolt
    {
        public BoltTemplate template { get; private set; }

        public override EntityType entityType { get { return EntityType.Bolt; } }

        private float maxPosZ;

        public bool Initialize(int id, IGame game, BoltTemplate template, int lineId, Vector3 from)
        {
            if (!Initialize(id, game))
                return false;

            this.template = template;
            this.lineId = lineId;

            transform.position = from;

            maxPosZ = MagicNumbers.FieldSizeZ;

            return true;
        }

        protected override void Update()
        {
            base.Update();

            if (!game.isRunning)
                return;

            if (posZ < maxPosZ)
            {
                var stepDist = template.moveSpeed * Time.deltaTime;
                var newPos = transform.position + new Vector3(0, 0, stepDist);
                transform.position = newPos;

                var victim = FindVictim();
                if (victim != null)
                {
                    // attack or generate area
                    ApplyAction(victim);
                }

                if (newPos.z >= maxPosZ)
                {
                    // self-destroy without activation
                    //Debug.Log("Bolt arrived");
                    Die();
                }
            }
        }

        private ICreature FindVictim()
        {
            var victim = game.entityManager.entities[EntityType.Creature]
                .Where(c => c.lineId == lineId && c.posZ <= posZ)
                .OrderBy(c=>c.posZ)
                .Cast<ICreature>()
                .FirstOrDefault();

            return victim;
        }

        // todo: attack or generate area
        private void ApplyAction(ICreature victim)
        {
            //Debug.Log("Bolt hit " + victim.id + " " + template.attackPower);

            victim.ApplyDamage(template.attackPower);

            if (!string.IsNullOrEmpty(template.attackAreaId))
            {
                // spawn area
                var area = game.entityBuilder.CreateArea(template.attackAreaId, lineId, transform.position);
                game.entityManager.AddEntity(area);
            }

            Die();
        }
    }
}
