﻿using System.Linq;
using TdGame.Data;
using TdGame.Interfaces;
using UnityEngine;

namespace TdGame.Impl
{
    public class Area : Entity, IArea
    {
        public AreaTemplate template { get; private set; }

        public override EntityType entityType { get { return EntityType.Area; } }

        public float lifetime { get; private set; }

        private float actionCooldownTime;

        public void Initialize(int id, IGame game, AreaTemplate template, int lineId, Vector3 pos)
        {
            this.template = template;
            this.lineId = lineId;
            this.lifetime = template.lifetime;

            transform.position = pos;

            Initialize(id, game);
        }

        protected override void Update()
        {
            base.Update();

            //if (isDisposed) return;

            if (!game.isRunning)
                return;

            actionCooldownTime -= Time.deltaTime;

            if (actionCooldownTime <= 0)
            {
                ApplyAction();
            }

            lifetime -= Time.deltaTime;

            if (lifetime <= 0)
                Die();
        }

        protected void ApplyAction()
        {
            // find victims around
            var victims = game.entityManager.entities[EntityType.Creature]
                .Where(c => c.lineId == lineId && Mathf.Abs(c.posZ - posZ) <= template.size * 0.5f)
                .Cast<ICreature>()
                .ToList();

            // todo: add fx

            foreach (var victim in victims)
            {
                victim.ApplyDamage(template.attackPower);
            }
        }
    }
}
