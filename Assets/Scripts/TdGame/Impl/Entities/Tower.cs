﻿using System.Collections;
using Pump.Unity;
using TdGame.Data;
using TdGame.Interfaces;
using UnityEngine;

namespace TdGame.Impl
{
    public class Tower : Entity, ITower
    {
        public override EntityType entityType { get { return EntityType.Tower; } }

        public TowerTemplate template { get; private set; }

        public int rowId { get; private set; }

        public bool isReloading { get { return attackCooldownTime > 0;} }

        public bool isChangingPos { get { return _changePosCoroutine != null; } }

        private float health;
        private float attackCooldownTime;

        private HealthUI healthUI;
        private ReloadUI reloadUI;

        private Coroutine _changePosCoroutine;
        private Vector3 _changePosFrom;
        private Vector3 _changePosTo;
        private float _changeTime;
        private float _changeTimeTotal;

        public bool Initialize(int id, IGame game, TowerTemplate template, int lineId, int rowId)
        {
            if (!Initialize(id, game))
                return false;

            this.template = template;
            this.lineId = lineId;
            this.rowId = rowId;

            health = template.health;

            var collider = gameObject.AddComponent<BoxCollider>();
            collider.isTrigger = true;
            collider.size = new Vector3(template.size, 1, template.size);

            transform.position = MagicNumbers.GetTowerPos(lineId, rowId);

            var prefab = Resources.Load<GameObject>(GamePrefabPath.BigHealthUI);
            var go = Instantiate(prefab);
            go.transform.SetParent(game.gameUI.transform, false);
            healthUI = go.AddComponent<HealthUI>();
            healthUI.Initialize(game, transform);
            healthUI.SetMaxHealth((int)template.health);
            healthUI.UpdatePosition();
            healthUI.HideFullHealth(true);

            prefab = Resources.Load<GameObject>(GamePrefabPath.ReloadUI);
            go = Instantiate(prefab);
            go.transform.SetParent(game.gameUI.transform, false);
            reloadUI = go.AddComponent<ReloadUI>();
            reloadUI.Initialize(game, transform);
            reloadUI.UpdatePosition();

            return true;
        }

        //todo: add animation
        public bool TryShoot()
        {
            if (attackCooldownTime > 0)
                return false;

            attackCooldownTime = template.attackCooldown;

            var pos = transform.position;
            pos.y = MagicNumbers.BoltY;

            var bolt = game.entityBuilder.CreateBolt(template.attackBoltId, lineId, pos);
            game.entityManager.AddEntity(bolt);

            return true;
        }

        //todo: add animation
        public void ChangePosition(int lineId, int rowId)
        {
            this.lineId = lineId;
            this.rowId = rowId;

            var newPos = MagicNumbers.GetTowerPos(lineId, rowId);

            _changePosCoroutine = StartCoroutine(StartChangePosCoroutine(newPos));
        }

        private IEnumerator StartChangePosCoroutine(Vector3 newPos)
        {
            _changeTime = 0;
            _changeTimeTotal = MagicNumbers.TowerSwapTime;
            _changePosFrom = transform.position;
            _changePosTo = newPos;

            while (_changeTime < _changeTimeTotal)
            {
                yield return new WaitForEndOfFrame();

                _changeTime = Mathf.Min(_changeTime + Time.deltaTime, _changeTimeTotal);

                transform.position = Vector3.Lerp(_changePosFrom, _changePosTo,
                    TweenUtils.EaseInCubic(_changeTime / _changeTimeTotal));
            }
        }

        // return true if tower was killed
        public bool ApplyDamage(float attackPower)
        {
            health = Mathf.Max(0, health - attackPower);

            if (health <= 0)
            {
                Die();
                return true;
            }

            return false;
        }

        protected override void Update()
        {
            base.Update();

            if (!game.isRunning)
                return;

            if (attackCooldownTime > 0)
            {
                attackCooldownTime = Mathf.Max(0, attackCooldownTime - Time.deltaTime);
            }

            reloadUI.UpdateCooldown(attackCooldownTime, template.attackCooldown);
            healthUI.SetHealth((int)health);
        }

        public override void Dispose()
        {
            base.Dispose();

            if (isChangingPos)
                StopCoroutine(_changePosCoroutine);

            if (reloadUI != null)
            {
                reloadUI.Dispose();
                Destroy(reloadUI.gameObject);
                reloadUI = null;
            }

            if (healthUI != null)
            {
                healthUI.Dispose();
                Destroy(healthUI.gameObject);
                healthUI = null;
            }
        }
    }
}
