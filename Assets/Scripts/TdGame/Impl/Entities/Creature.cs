﻿using System.Linq;
using TdGame.Data;
using TdGame.Interfaces;
using UnityEngine;

namespace TdGame.Impl
{
    public class Creature : Entity, ICreature
    {
        public CreatureTemplate template { get; private set; }

        public override EntityType entityType { get { return EntityType.Creature; } }

        public float health { get; private set; }

        private float attackCooldownTime;

        private bool isMoving;

        private HealthUI healthUI;

        private ITower victim;

        public void Initialize(int id, IGame game, CreatureTemplate template, int lineId, Vector3 pos)
        {
            this.template = template;
            this.lineId = lineId;
            this.health = template.health;

            transform.position = pos;

            Initialize(id, game);

            var prefab = Resources.Load<GameObject>(GamePrefabPath.SmallHealthUI);
            var go = Instantiate(prefab);
            go.transform.SetParent(game.gameUI.transform, false);
            healthUI = go.AddComponent<HealthUI>();
            healthUI.Initialize(game, transform);
            healthUI.SetHealth(0);
            healthUI.SetMaxHealth((int)template.health);
            healthUI.UpdatePosition();
            healthUI.HideFullHealth(true);
        }

        // return true if creature was killed
        public bool ApplyDamage(float attackPower)
        {
            health = Mathf.Max(0, health - attackPower);

            if (health <= 0)
            {
                game.score += template.score; // todo: hack score

                Die();
                return true;
            }

            return false;
        }

        protected override void Update()
        {
            base.Update();

            if (!game.isRunning)
                return;

            // target is dead or too fara to attack
            if (victim != null && (victim.isDisposed
                || (victim.posZ + victim.template.size * 0.5f) < (posZ + template.size * 0.5f)))
                victim = null;

            if (attackCooldownTime > 0)
            {
                attackCooldownTime = Mathf.Max(0, attackCooldownTime - Time.deltaTime);
            }
            else
            {
                if (victim == null)
                    victim = FindVictim();

                if (victim != null)
                    ApplyAttack();
            }

            if (victim == null && posZ > 0)
            {
                var stepDist = template.moveSpeed * Time.deltaTime;
                transform.position = transform.position - new Vector3(0, 0, stepDist);
            }


            healthUI.SetHealth((int)health);
        }

        private ITower FindVictim()
        {
            var victim = game.entityManager.entities[EntityType.Tower]
                .Where(c => c.lineId == lineId && c.posZ >= posZ)
                .OrderByDescending(c => c.posZ)
                .Cast<ITower>()
                .FirstOrDefault();

            return victim;
        }

        private void ApplyAttack()
        {
            //Debug.Log("ApplyAttack " + victim.id + " " + template.attackPower);

            attackCooldownTime = template.attackCooldown;

            victim.ApplyDamage(template.attackPower);
        }

        public override void Dispose()
        {
            base.Dispose();

            if (healthUI != null)
            {
                healthUI.Dispose();
                Destroy(healthUI.gameObject);
                healthUI = null;
            }
        }
    }
}
