﻿using System;
using TdGame.Interfaces;
using UnityEngine;

namespace TdGame.Impl
{
    public abstract class Entity : MonoBehaviour, IEntity
    {
        public event Action<IEntity> onDisposed = (entity) => { };

        public int id { get; private set; }

        public abstract EntityType entityType { get; }

        public int lineId { get; protected set; }

        public float posZ
        {
            get { return transform.position.z; }
        }

        protected IGame game { get; private set; }

        protected bool isInitialized { get; private set; }

        public bool isDisposed { get; private set; }

        protected virtual bool Initialize(int id, IGame game)
        {
            if (isInitialized || isDisposed)
                return false;

            isInitialized = true;
            this.id = id;
            this.game = game;

            return true;
        }

        protected virtual void Update()
        {

        }

        protected virtual void Die()
        {
            Dispose();
        }

        public virtual void Dispose()
        {
            if (isDisposed)
                return;

            // raise event
            onDisposed.Invoke(this);

            isDisposed = true;

            // cleanup here

            game = null;

            Destroy(gameObject);
        }
    }
}
