﻿using System;
using TdGame.Data;
using TdGame.Interfaces;
using UnityEngine;

namespace TdGame.Impl
{
    public class EntityBuilder : MonoBehaviour, IEntityBuilder
    {
        public bool isDisposed { get; private set; }

        private IGame game;

        private int nextEntityId;

        public void Initialize(IGame game)
        {
            this.game = game;
            nextEntityId = 1;
        }

        public ITower CreateTower(string templateId, int lineId, int rowId)
        {
            var entityTemplate = game.staticGameData.towers[templateId];

            var go = GameObject.Instantiate(Resources.Load<GameObject>(entityTemplate.prefabPath));
            go.name = templateId;

            var tower = go.AddComponent<Tower>();
            tower.Initialize(nextEntityId++, game, entityTemplate, lineId, rowId);

            return tower;
        }

        public ICreature CreateCreature(string templateId, int lineId, Vector3 pos)
        {
            var entityTemplate = game.staticGameData.creatures[templateId];

            var go = GameObject.Instantiate(Resources.Load<GameObject>(entityTemplate.prefabPath));
            go.name = templateId;
            go.transform.rotation = Quaternion.Euler(0,180,0);

            var creature = go.AddComponent<Creature>();
            creature.Initialize(nextEntityId++, game, entityTemplate, lineId, pos);

            return creature;
        }

        public IBolt CreateBolt(string templateId, int lineId, Vector3 pos)
        {
            var entityTemplate = game.staticGameData.bolts[templateId];

            var go = GameObject.Instantiate(Resources.Load<GameObject>(entityTemplate.prefabPath));
            go.name = templateId;

            var bolt = go.AddComponent<Bolt>();
            bolt.Initialize(nextEntityId++, game, entityTemplate, lineId, pos);

            return bolt;
        }

        public IArea CreateArea(string templateId, int lineId, Vector3 pos)
        {
            var entityTemplate = game.staticGameData.areas[templateId];

            var go = GameObject.Instantiate(Resources.Load<GameObject>(entityTemplate.prefabPath));
            go.name = templateId;

            var area = go.AddComponent<Area>();
            area.Initialize(nextEntityId++, game, entityTemplate, lineId, pos);

            return area;
        }

        public ICreatureSpawner CreateSpawner(SpawnerTemplate entityTemplate, int lineId)
        {
            var go = new GameObject("Spawner");
            var spawner = go.AddComponent<CreatureSpawner>();
            spawner.Initialize(game, entityTemplate, lineId);

            return spawner;
        }

        public virtual void Dispose()
        {
            if (isDisposed)
                return;

            isDisposed = true;

            // cleanup here

            game = null;
        }
    }
}
