﻿using System.Collections;
using System.Collections.Generic;
using TdGame.Interfaces;
using UnityEngine;

namespace TdGame.Impl
{
    public class PlayerController : MonoBehaviour
    {
        public bool isDisposed { get; private set; }

        private IGame game;

        private TouchInputListener touchInputListener;

        private ITower draggingTower;
        private Vector3 startDragTouchPos;

        public void Initialize(IGame game)
        {
            this.game = game;

            touchInputListener = gameObject.AddComponent<TouchInputListener>();
            touchInputListener.OnClickCallback += OnClick;
            touchInputListener.OnDragStartedCallback += OnDragStarted;
            touchInputListener.OnDragCallback += OnDrag;
            touchInputListener.OnDragFinishedCallback += OnDragFinished;
        }

        void Update()
        {
            if (draggingTower != null && draggingTower.isDisposed)
            {
                draggingTower = null;
            }
        }

        private void OnClick()
        {
            draggingTower = null;

            var tower = GetTowerUnderTouchPos(Input.mousePosition);

            if (tower != null)
                tower.TryShoot();
        }

        private void OnDragStarted()
        {
            startDragTouchPos = Input.mousePosition;
            draggingTower = GetTowerUnderTouchPos(startDragTouchPos);
        }

        private void OnDrag()
        {
            if (draggingTower == null) return;
            TrySwapTowers();
        }

        private void OnDragFinished()
        {
            if (draggingTower == null) return;
            TrySwapTowers();
            draggingTower = null;
        }

        private void TrySwapTowers()
        {
            if (draggingTower == null)
                return;

            var otherTower = GetTowerUnderTouchPos(Input.mousePosition);

            if (otherTower == null || otherTower == draggingTower)
                return;

            SwapTowers(draggingTower, otherTower);
            draggingTower = null;
        }

        public void SwapTowers(ITower tower1, ITower tower2)
        {
            var l1 = tower1.lineId;
            var r1 = tower1.rowId;
            var l2 = tower2.lineId;
            var r2 = tower2.rowId;

            tower1.ChangePosition(l2, r2);
            tower2.ChangePosition(l1, r1);
        }

        private ITower GetTowerUnderTouchPos(Vector3 touchPos)
        {
            // The ray to the touched object in the world
            Ray ray = Camera.main.ScreenPointToRay(touchPos);

            // Your raycast handling
            var hits = Physics.RaycastAll(ray.origin, ray.direction);
            foreach (var hitInfo in hits)
            {
                var tower = hitInfo.collider.GetComponent<ITower>();
                if (tower != null)
                {
                    return tower;
                }
            }

            return null;
        }

        public virtual void Dispose()
        {
            if (isDisposed)
                return;

            isDisposed = true;

            // cleanup here

            game = null;
        }
    }
}
