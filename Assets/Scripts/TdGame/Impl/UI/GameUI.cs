﻿using System;
using Pump.Unity;
using TdGame.Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TdGame.Impl
{
    public class GameUI : MonoBehaviour, IGameUI
    {
        public event Action onMenuButtonClicked = () => { };

        public Canvas canvas { get; private set; }

        private TextMeshProUGUI scoreText;
        private TextMeshProUGUI currentWaveText;
        private TextMeshProUGUI currentWaveTimeText;

        private Button menuButton;

        private IGame game;

        public void Initialize(IGame game, Canvas canvas)
        {
            this.game = game;
            this.canvas = canvas;

            var topPanel = transform.Find("TopPanel");

            scoreText = topPanel.Find("Score").GetComponent<TextMeshProUGUI>();

            currentWaveText = topPanel.Find("WaveCounter").GetComponent<TextMeshProUGUI>();

            currentWaveTimeText = topPanel.Find("WaveTime").GetComponent<TextMeshProUGUI>();

            menuButton = topPanel.Find("MenuButton").GetComponent<Button>();
            menuButton.onClick.AddListener(OnMenuButtonClicked);
        }

        private void OnMenuButtonClicked()
        {
            onMenuButtonClicked.Invoke();
        }

        void Update()
        {
            if (game == null || !game.isRunning)
                return;

            scoreText.text = game.score.ToString();
            currentWaveText.text = (game.spawnWaves.currentWave+1) + "/" + game.spawnWaves.totalWavesCount;
            currentWaveTimeText.text = GameObjectUtils.FormatTime(
                    (int) (game.spawnWaves.currentWaveTimeTotal - game.spawnWaves.currentWaveTime));
        }

        public void Dispose()
        {
            game = null;
            canvas = null;

            scoreText = null;
            currentWaveText = null;
            currentWaveTimeText = null;
        }
    }
}
