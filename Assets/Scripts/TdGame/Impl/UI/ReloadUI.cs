﻿using Pump.Unity;
using TdGame.Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TdGame.Impl
{
    public class ReloadUI : MonoBehaviour
    {
        public Transform target;

        private float _lastCooldown;

        private GameObject viewChild;

        protected TextMeshProUGUI _reloadTimer;
        protected Image _reloadBar;

        private Canvas canvas;
        private Camera camera;

        public void Initialize(IGame game, Transform target)
        {
            _lastCooldown = -1;

            canvas = game.gameUI.canvas;
            camera = Camera.main;

            viewChild = transform.GetChild(0).gameObject;

            _reloadTimer = viewChild.transform.Find("Time").GetComponent<TextMeshProUGUI>();

            _reloadBar = viewChild.transform.Find("Cooldown").GetComponent<Image>();

            viewChild.SetActive(false);

            this.target = target;
        }

        public void UpdateCooldown(float cooldown, float maxCooldown)
        {
            if (cooldown == _lastCooldown)
                return;

            _reloadBar.fillAmount = 1 - cooldown / maxCooldown;
            //_reloadTimer.text = GameObjectUtils.FormatTime((int)cooldown);
            _reloadTimer.text = ((int)cooldown).ToString();

            viewChild.SetActive(cooldown>0);
        }

        public virtual void UpdatePosition()
        {
            if (target == null)
                return;

            transform.position = RectTransformUtility.WorldToScreenPoint(camera, target.position)
                                     + new Vector2(0, 1 * canvas.scaleFactor);
        }

        void Update()
        {
            UpdatePosition();
        }

        public void Dispose()
        {
            viewChild = null;

            target = null;

            canvas = null;
            camera = null;
        }
    }
}
