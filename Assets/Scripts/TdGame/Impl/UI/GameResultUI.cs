﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TdGame.Impl
{
    public class GameResultUI : MonoBehaviour
    {
        public event Action onContinueButtonClicked = () => { };
        public event Action onRestartButtonClicked = () => { };

        public Button continueButton;
        public Button restartButton;

        public Button outsideArea;

        public GameObject winCaption;
        public GameObject loseCaption;

        public TextMeshProUGUI scoreText;
        public TextMeshProUGUI waveText;

        void Awake()
        {
            continueButton.onClick.AddListener(OnContinueButtonClicked);
            restartButton.onClick.AddListener(OnRestartButtonClicked);
        }

        private void OnContinueButtonClicked()
        {
            onContinueButtonClicked.Invoke();
        }

        private void OnRestartButtonClicked()
        {
            onRestartButtonClicked.Invoke();
        }
    }
}
