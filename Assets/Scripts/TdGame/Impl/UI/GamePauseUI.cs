﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace TdGame.Impl
{
    public class GamePauseUI : MonoBehaviour
    {
        public event Action onHomeButtonClicked = () => { };
        public event Action onContinueButtonClicked = () => { };
        public event Action onRestartButtonClicked = () => { };

        public Button homeButton;
        public Button continueButton;
        public Button restartButton;

        public Button outsideArea;

        void Awake()
        {
            homeButton.onClick.AddListener(OnHomeButtonClicked);
            continueButton.onClick.AddListener(OnContinueButtonClicked);
            restartButton.onClick.AddListener(OnRestartButtonClicked);
            outsideArea.onClick.AddListener(OnContinueButtonClicked);
        }

        private void OnHomeButtonClicked()
        {
            onHomeButtonClicked.Invoke();
        }

        private void OnContinueButtonClicked()
        {
            onContinueButtonClicked.Invoke();
        }

        private void OnRestartButtonClicked()
        {
            onRestartButtonClicked.Invoke();
        }
    }
}
