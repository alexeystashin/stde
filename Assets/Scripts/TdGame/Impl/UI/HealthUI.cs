﻿using TdGame.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace TdGame.Impl
{
    public class HealthUI : MonoBehaviour
    {
        public Transform target;

        public bool _hideFullHealth;

        public int _health;

        public int _maxHealth;

        public float _size;
        public float _height;

        protected Image _healthBack;
        protected Image _healthBar;

        protected bool _isEnemy;


        private GameObject viewChild;

        private Canvas canvas;
        private Camera camera;

        public void Initialize(IGame game, Transform target)
        {
            viewChild = transform.GetChild(0).gameObject;

            canvas = game.gameUI.canvas;
            camera = Camera.main;

            _healthBack = viewChild.transform.Find("Health").GetComponent<Image>();
            _healthBar = viewChild.transform.Find("Health/HealthBar").GetComponent<Image>();

            canvas = game.gameUI.canvas;

            viewChild.SetActive(false);

            this.target = target;
        }

        public void SetIsEnemy(bool isEnemy)
        {
            _isEnemy = isEnemy;
            SetColor(isEnemy ? new Color32(255, 40, 40, 255) : new Color32(30, 100, 255, 255));
        }

        private void SetColor(Color healthColor)
        {
            _healthBar.color = healthColor;
        }

        public void HideFullHealth(bool hide)
        {
            _hideFullHealth = hide;
        }

        public void SetSizeAndHeight(float size, float height)
        {
            _size = size;
            _height = height;
            _healthBack.rectTransform.localScale = new Vector3(size, 1, 1);
        }

        public void SetHealth(int health)
        {
            _health = health;

            _healthBar.fillAmount = _maxHealth > 0 ? health / (float)_maxHealth : 1;

            viewChild.SetActive(!_hideFullHealth || _health < _maxHealth);
        }

        public void SetMaxHealth(int maxHealth)
        {
            _maxHealth = maxHealth;

            viewChild.SetActive(!_hideFullHealth || _health < _maxHealth);
        }

        public virtual void UpdatePosition()
        {
            if (target == null)
                return;

            transform.position = RectTransformUtility.WorldToScreenPoint(Camera.main, target.position)
                                     + new Vector2(0, _height * 20 * canvas.scaleFactor);
        }

        void Update()
        {
            UpdatePosition();
        }

        public void Dispose()
        {
            _healthBack = null;
            _healthBar = null;

            target = null;

            canvas = null;
            camera = null;
        }
    }
}
